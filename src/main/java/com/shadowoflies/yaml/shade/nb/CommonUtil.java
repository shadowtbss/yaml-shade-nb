/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import java.util.MissingResourceException;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;

/**
 * Simple set of common utility functions.
 *
 * @author Gavin Boshoff
 */
public final class CommonUtil {

    /**
     * Performs the retrieval of localized values from the available plugin bundle. It's good practice to process all
     * values that could be localized through here in order to later easily perform localization, if needed.
     *
     * @param key The {@code String} key for the value to be localized
     *
     * @return The localized value, if present, otherwise the original key provided
     */
    static String getLocalized(String key) {
        String localized;

        try {
            localized = NbBundle.getMessage(CommonUtil.class, key);
        } catch (MissingResourceException mre) {
            localized = key;
        }

        return localized;
    }

    /**
     * Performs a check on the given {@code FileObject} to determine if it is considered a {@code yaml} file.
     *
     * @param file The {@code FileObject} to be checked
     *
     * @return {@code true} if the object is determined to represent a {@code yaml} file, {@code false} otherwise
     */
    static boolean isYamlFile(FileObject file) {
        return file != null
                && !file.isFolder()
                && equalsAnyIgnoreCase(file.getExt(), "yaml", "yml");
    }

    /**
     * Checks whether or not the provided value is equal to any of the potential matches provided, ignoring case. The
     * check is {@code null} safe.
     *
     * @param test The value to be checked
     * @param matches The potential values to be matched
     *
     * @return {@code true} if any of the potential matches are matched, {@code false} otherwise
     */
    private static boolean equalsAnyIgnoreCase(String test, String... matches) {
        for (String match : matches) {
            if (equalsIgnoreCase(test, match)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether or not the provided value is equal to the potential match value provided, ignoring case. The
     * check is {@code null} safe and will also result in a match if both provided values are {@code null}.
     *
     * @param test The value to be checked
     * @param match The potential value to be matched
     *
     * @return {@code true} if the test and match values are equal, {@code false} otherwise
     */
    @SuppressWarnings("StringEquality")
    private static boolean equalsIgnoreCase(String test, String match) {
        if (test == match) {
            return true;
        }

        if (test == null || match == null) {
            return false;
        }

        return test.equalsIgnoreCase(match);
    }

    /**
     * Hide utility class constructor.
     */
    private CommonUtil() {}
}
