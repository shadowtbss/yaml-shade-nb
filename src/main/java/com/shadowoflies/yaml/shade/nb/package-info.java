
@TemplateRegistration(
        folder = "Other",
        content = "SchemaYamlTemplate.yaml",
        displayName = "YAML File (Schema Managed)",
        description = "./SchemaYamlFileDescription.html",
        position = 852
)
package com.shadowoflies.yaml.shade.nb;

import org.netbeans.api.templates.TemplateRegistration;
