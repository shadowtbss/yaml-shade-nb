/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.HashSet;
import org.openide.filesystems.FileChangeAdapter;
import org.openide.filesystems.FileEvent;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.windows.OnShowing;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

/**
 * A handler that takes care of handling and dispatching events associated with modifications to {@code yaml} files
 * being edited in order to scan them.
 *
 * @author Gavin Boshoff
 */
@OnShowing
public class YamlModificationHandler implements Runnable {

    /** Identifier for the file opened event. */
    private static final String FILE_OPENED = "opened";

    /** Single instance of the file change handler added as listener to monitored files. */
    private static final FileChangeHandler CHANGE_HANDLER;

    static {
        CHANGE_HANDLER = new FileChangeHandler();
    }

    /**
     * Default constructor.
     */
    public YamlModificationHandler() {}

    /**
     * Execution primarily registers the necessary listener to the IDE.
     *
     * @see YamlModificationHandler.Listener
     */
    @Override
    public void run() {
        this.registerListener();
    }

    /**
     * Registers a listener on the IDE to track actions applied to files.
     *
     * @see YamlModificationHandler.Listener
     */
    private void registerListener() {
        WindowManager.getDefault().getRegistry()
                .addPropertyChangeListener(new YamlModificationHandler.Listener());
    }

    /**
     * A listener that can be attached to the IDE in order to listen for certain events. The main event(s) this listener
     * is interested in are focused on being able to detect when a {@code yaml} file is being prepared by the IDE for
     * use by the user.
     * <p>
     * When a file is detected that the listener is interested in, it will attach another listener to the file itself
     * in order to handle events associated with the file.</p>
     */
    static class Listener implements PropertyChangeListener {

        /** Localize constructor. */
        private Listener() {}

        /**
         * {@inheritDoc}
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(FILE_OPENED)) {
                HashSet<TopComponent> components;

                components = ((HashSet<TopComponent>) evt.getNewValue());
                components.stream()
                        .map(comp -> comp.getLookup().lookup(DataObject.class))
                        .filter(dao -> dao != null)
                        .map(dao -> dao.getPrimaryFile())
                        .filter(file -> file != null && CommonUtil.isYamlFile(file))
                        .distinct()
                        .forEach(file -> {
                            YamlSchemaValidator.handle(file);

                            // Remove the file change listener, if it has been added before
                            file.removeFileChangeListener(CHANGE_HANDLER);

                            // Add the file change listener
                            file.addFileChangeListener(CHANGE_HANDLER);
                        });
            }
        }
    }

    // Stateless
    /**
     * An effectively stateless file change handler that will be added as a file change listener to all {@code yaml}
     * files that are detected and need to be potentially processed.
     * <p>
     * The reason for being effectively stateless is in order to reduce the number of listeners created. Being
     * stateless, the handler is merely responsible for dispatching the necessary actions based on the events.
     */
    private static class FileChangeHandler extends FileChangeAdapter {

        private volatile FileObject previousFo;
        private volatile long previousTime;

        /** Localize constructor. */
        private FileChangeHandler() {
            this.previousTime = Long.MIN_VALUE;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void fileChanged(FileEvent fe) {
            boolean execHandle = true;

            // The IDE triggers this event from multiple sources
            // We try and reduce repeated executions
            if (fe.getTime() - previousTime  < 2L && previousFo == fe.getFile()) {
                execHandle = false;
            }

            previousTime = fe.getTime();
            previousFo = fe.getFile();

            if (execHandle) {
                YamlSchemaValidator.handle(fe.getFile());
            }
        }
    }
}
