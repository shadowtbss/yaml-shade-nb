/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import org.openide.text.Annotation;

/**
 * A YAML Schema Violation annotation containing the details associated with a schema definition violation to be
 * provided as feedback to the user.
 *
 * @see Annotation
 *
 * @author Gavin Boshoff
 */
public class YamlSchemaViolation extends Annotation {

    /** Required to group and identify the annotations. */
    static final String ANNOTATION_TYPE = "yaml-schema-violation";

    private final String message;
    private final int line;

    /**
     * Creates an annotation instance associated with a given line and presenting a given message.
     *
     * @param line The line at which the annotation is present and relevant
     * @param message The message to describe the reason for the annotation
     */
    YamlSchemaViolation(int line, String message) {
        this.line = line;
        this.message = message;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getAnnotationType() {
        return ANNOTATION_TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getShortDescription() {
        return this.message;
    }

    /**
     * The line number at which the annotation is present or relevant.
     *
     * @return The line number
     */
    public int getLineNumber() {
        return this.line;
    }
}
