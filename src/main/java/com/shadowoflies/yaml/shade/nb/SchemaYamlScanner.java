/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.netbeans.spi.tasklist.FileTaskScanner;
import org.netbeans.spi.tasklist.Task;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;

/**
 * Provides the task associated with scanning {@code yaml} files being edited.
 *
 * @author Gavin Boshoff
 */
public class SchemaYamlScanner extends FileTaskScanner {

    private static final Logger LOGGER = Logger.getLogger(SchemaYamlScanner.class.getName());

    /** The group name for general netbeans errors. */
    private static final String GROUP_NAME = "nb-tasklist-error";

    /**
     * Creates an instance of the scanner and will be how NetBeans instantiates the instance to be used during runtime.
     *
     * @return The {@code SchemaYamlScanner} instance used by the IDE
     */
    public static SchemaYamlScanner create() {
        return new SchemaYamlScanner(
                CommonUtil.getLocalized("TaskScanner.Label"),
                CommonUtil.getLocalized("TaskScanner.Description")
        );
    }

    /**
     * Constructs a new instance of the scanner given the display name and description.
     *
     * @param displayName The {@code String} display name for the scanner
     * @param description The {@code String} description for the scanner
     */
    public SchemaYamlScanner(String displayName, String description) {
        // TODO: GBO - Possible to make this constructor private
        super(displayName, description, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<? extends Task> scan(FileObject resource) {
        List<YamlSchemaViolation> violations;
        LinkedList<Task> tasks;

        tasks = new LinkedList<>();
        if (CommonUtil.isYamlFile(resource)) {
            try {
                violations = YamlSchemaValidator.violations(DataObject.find(resource));

                for (YamlSchemaViolation violation : violations) {
                    tasks.add(Task.create(resource, GROUP_NAME, violation.getShortDescription(), violation.getLineNumber() + 1));
                }
            } catch (DataObjectNotFoundException donfe) {
                LOGGER.log(Level.SEVERE, "Failed to find data object for resource : " + resource, donfe);
            }
        }

        return tasks;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void attach(Callback callback) {}
}
