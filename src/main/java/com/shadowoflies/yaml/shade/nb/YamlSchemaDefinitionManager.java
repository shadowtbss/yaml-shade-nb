/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpStatus;
import org.everit.json.schema.Schema;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.openide.filesystems.FileObject;

/**
 * Serves for the management of schema definitions that have been used during the current IDE session. The definition
 * manager takes responsibility for the following:
 * <ul>
 * <li>Check whether or not a schema is associated with a given file</li>
 * <li>Check whether or not the schema location is properly formulated</li>
 * <li>Retrieve the schema definition from the specified location</li>
 * <li>Cache the schema definition for continuous use</li>
 * </ul>
 * The single definition manager handles all schema definitions, even if there are different files using different
 * schemas.
 *
 * @author Gavin Boshoff
 */
public class YamlSchemaDefinitionManager {

    private static final Logger LOGGER = Logger.getLogger(YamlSchemaDefinitionManager.class.getName());

    /** Pattern to find the schema location from a given line in a file */
    private static final Pattern SCHEMA_URL_PTRN = Pattern.compile("yaml-language-server: ?\\$schema=(.*)");

    /**
     * Potential result values that can, in the event of a negative result, also be handled and displayed as a violation
     * within the associated {@code yaml} file.
     */
    enum SchemaLoadResult {
        /** Neutral: No schema value */
        NO_SCHEMA,
        /** Positive: Schema successfully loaded from either resource or cache */
        SCHEMA_LOADED,
        /** Neutral: Temporary value for schema being loaded asynchronously */
        SCHEMA_LOADING,
        /** Negative: Location value provided for schema is considered invalid */
        INVALID_URL,
        /** Negative: Error associated with schema loaded through HTTP(S) */
        HTTP_ERROR,
        /** Negative: Error associated with schema loaded through FTP */
        FTP_ERROR,
        /** Negative: Schema loaded through FILE protocol could not be found */
        FNF_ERROR,
        /** Negative: Error associated with schema loaded through FILE protocol */
        FILE_ERROR,
        /** Negative: Any other unexpected error */
        OTHER_ERROR
    };

    /** Set of allowed schemes for schema location. These are the ones currently being handled */
    private static final String[] ALLOWED_SCHEMES = {"http", "https", "ftp", "file"};

    private final Map<String, Schema> schemaCache;
    private final UrlValidator urlValidator;

    /**
     * Creates a new instance of the {@code YamlSchemaDefinitionManager}. It is expected that only 1 would be created in
     * any given IDE session, but it is possible to create additional instances.
     */
    YamlSchemaDefinitionManager() {
        this.schemaCache = new HashMap<>();
        this.urlValidator = new UrlValidator(ALLOWED_SCHEMES);
    }

    /**
     * <p>Instructs the manager to load the schema, if any, associated with the provided {@code FileObject}. This method
     * can be called frequently, as it will not make calls to the external schema location once it has been retrieved.
     * </p>
     * <p>In order to reduce blocking in the IDE, a callback can be specified which will be called once the schema has
     * been loaded. This is however only relevant when the schema has not been loaded before and means it should,
     * generally speaking, only be called once per IDE session, per schema location.
     * </p>
     *
     * @param file The file object containing a potentially schema constrained {@code yaml} document
     * @param asyncLoadCallback The function to be called if the schema is being loaded asynchronously
     *
     * @return The result of the load action
     */
    SchemaLoadResult loadSchema(FileObject file, Consumer<SchemaLoadResult> asyncLoadCallback) {
        String schemaLocation;

        schemaLocation = extractSchemaLocation(file);

        if (schemaLocation != null) {
            if (!this.schemaCache.containsKey(schemaLocation)) {
                if (!this.urlValidator.isValid(schemaLocation)) {
                    return SchemaLoadResult.INVALID_URL;
                }

                return loadSchema(schemaLocation, asyncLoadCallback);
            } else {
                return SchemaLoadResult.SCHEMA_LOADED;
            }
        }

        return SchemaLoadResult.NO_SCHEMA;
    }

    /**
     * Gets the cached schema for the specified {@code FileObject}. This method is expected to only be called if a
     * positive result was received when calling {@link #loadSchema(org.openide.filesystems.FileObject)}.
     *
     * @param file The file object for which the associated schema definition should be returned
     *
     * @return The schema definition associated with the specified file object
     */
    Schema getSchema(FileObject file) {
        return this.schemaCache.get(extractSchemaLocation(file));
    }

    /**
     * Extract the schema location value from the file object content.
     *
     * @param file The {@code FileObject} potentially containing a schema definition reference
     *
     * @return The schema location value {@code String}
     */
    private String extractSchemaLocation(FileObject file) {
        try {
            Matcher matcher;

            for (String line : file.asLines()) {
                if (line.length() > 0 && line.charAt(0) == '#') {
                    matcher = SCHEMA_URL_PTRN.matcher(line);
                    if (matcher.find()) {
                        return matcher.group(1);
                    }
                }
            }
        } catch (IOException ioe) {
            LOGGER.log(Level.SEVERE, "Failed to read file to extract schema location", ioe);
        }

        return null;
    }

    /**
     * Load the schema given the location. The location value is expected to already be considered well-formatted, but
     * could still be invalid. From here, the schema location will be checked for a scheme/protocol and the schema
     * definition retrieved accordingly.
     *
     * @param schemaLocation The location where the schema definition can be found
     * @param asyncLoadCallback The function to be called if the schema has been loaded asynchronously
     *
     * @return A result representing the outcome of the load
     */
    private SchemaLoadResult loadSchema(String schemaLocation, Consumer<SchemaLoadResult> asyncLoadCallback) {
        AsyncSchemaLoader loader;
        String locationScheme;

        locationScheme = schemaLocation.substring(0, schemaLocation.indexOf(':'));
        loader = new AsyncSchemaLoader(schemaLocation, asyncLoadCallback);

        switch (locationScheme) {
            case "http":
            case "https":
                new Thread(loader
                        .withSchemaLoader(this::loadFromUrl))
                        .start();
                return SchemaLoadResult.SCHEMA_LOADING;
            case "ftp":
                new Thread(loader
                        .withSchemaLoader(this::loadFromFtp))
                        .start();
                return SchemaLoadResult.SCHEMA_LOADING;
            case "file":
                new Thread(loader
                        .withSchemaLoader(this::loadFromFile))
                        .start();
                return SchemaLoadResult.SCHEMA_LOADING;
            default:
                return SchemaLoadResult.INVALID_URL;
        }
    }

    /**
     * Attempts to load the schema definition from a given URL, typically used for {@code http} or {@code https}
     * locations.
     *
     * @param schemaUrl The URL where the schema definition is expected to reside
     *
     * @return The result of the load action. This could either indicate that the schema was successfully loaded, or
     *      that an error occurred while attempting to load the schema
     */
    private SchemaLoadResult loadFromUrl(String schemaUrl) {
        try (CloseableHttpClient httpClient = HttpClients.createMinimal()) {
            try (CloseableHttpResponse response = httpClient.execute(new HttpGet(schemaUrl))) {

                if (response.getCode() == HttpStatus.SC_OK) {
                    JSONObject rawSchema;
                    HttpEntity entity;
                    Schema schema;

                    entity = response.getEntity();
                    rawSchema = new JSONObject(new JSONTokener(entity.getContent()));
                    schema = SchemaLoader.load(rawSchema);

                    this.schemaCache.put(schemaUrl, schema);

                    return SchemaLoadResult.SCHEMA_LOADED;
                } else {
                    return SchemaLoadResult.HTTP_ERROR;
                }
            }
        } catch (IOException ioe) {
            LOGGER.log(Level.INFO, "Failed to load schema from URL: " + schemaUrl, ioe);
            return SchemaLoadResult.HTTP_ERROR;
        }
    }

    /**
     * Attempts to load the schema definition from a given FTP endpoint, typically used for the {@code ftp} protocol.
     *
     * @param schemaFtp The FTP where the schema definition is expected to reside
     *
     * @return The result of the load action. This could either indicate that the schema was successfully loaded, or
     *      that an error occurred while attempting to load the schema
     */
    private SchemaLoadResult loadFromFtp(String schemaFtp) {
        InputStream inputStream = null;

        try {
            URLConnection urlConnection;
            JSONObject rawSchema;
            Schema schema;

            urlConnection = new URL(schemaFtp).openConnection();
            inputStream = urlConnection.getInputStream();

            rawSchema = new JSONObject(new JSONTokener(inputStream));
            schema = SchemaLoader.load(rawSchema);

            this.schemaCache.put(schemaFtp, schema);

            return SchemaLoadResult.SCHEMA_LOADED;
        } catch (MalformedURLException mue) {
            LOGGER.log(Level.INFO, "FTP Location malformed: " + schemaFtp, mue);
            return SchemaLoadResult.INVALID_URL;
        } catch (IOException ioe) {
            LOGGER.log(Level.INFO, "Failed to load schema from FTP: " + schemaFtp, ioe);
            return SchemaLoadResult.FTP_ERROR;
        } finally {
            silentClose(inputStream);
        }
    }

    /**
     * Attempts to load the schema definition from a given FILE location, typically used for the {@code file} protocol.
     *
     * @param schemaFile The File path where the schema definition is expected to reside
     *
     * @return The result of the load action. This could either indicate that the schema was successfully loaded, or
     *      that an error occurred while attempting to load the schema
     */
    private SchemaLoadResult loadFromFile(String schemaFile) {
        InputStream inputStream = null;

        try {
            URLConnection urlConnection;
            JSONObject rawSchema;
            Schema schema;

            urlConnection = new URI(schemaFile).toURL().openConnection();
            inputStream = urlConnection.getInputStream();

            rawSchema = new JSONObject(new JSONTokener(inputStream));
            schema = SchemaLoader.load(rawSchema);

            this.schemaCache.put(schemaFile, schema);

            return SchemaLoadResult.SCHEMA_LOADED;
        } catch (URISyntaxException | MalformedURLException ex) {
            LOGGER.log(Level.INFO, "FILE Location malformed: " + schemaFile, ex);
            return SchemaLoadResult.INVALID_URL;
        } catch (FileNotFoundException fnfe) {
            LOGGER.log(Level.INFO, "Failed to find schema from FILE: " + schemaFile, fnfe);
            return SchemaLoadResult.FNF_ERROR;
        } catch (IOException ioe) {
            LOGGER.log(Level.INFO, "Failed to load schema from FILE: " + schemaFile, ioe);
            return SchemaLoadResult.FILE_ERROR;
        } finally {
            silentClose(inputStream);
        }
    }

    /**
     * Silently closes one or more {@code Closeable}'s that were used. This method is {@code null}-safe and ignores any
     * {@code IOException} that may be raised.
     *
     * @param closeables The {@code Closeable}'s to be closed
     */
    private void silentClose(Closeable... closeables) {
        for (Closeable closeable : closeables) {
            if (closeable != null) {
                try {
                    closeable.close();
                } catch (IOException ioe) {
                    // Ignore error (silent)
                }
            }
        }
    }

    /**
     * A simple {@code Runnable} that chains the loading of a schema definition from the specified location to the
     * consumer that should process the load result once the schema is loaded.
     */
    private static class AsyncSchemaLoader implements Runnable {

        private final Consumer<SchemaLoadResult> loadedCallback;
        private final String schemaLocation;

        private Function<String, SchemaLoadResult> loader;

        private AsyncSchemaLoader(String schemaLocation, Consumer<SchemaLoadResult> loadedCallback) {
            this.schemaLocation = schemaLocation;
            this.loadedCallback = loadedCallback;
        }

        private AsyncSchemaLoader withSchemaLoader(Function<String, SchemaLoadResult> loader) {
            this.loader = loader;
            return this;
        }

        @Override
        public void run() {
            loadedCallback.accept(this.loader.apply(schemaLocation));
        }
    }
}
