/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import com.google.re2j.Matcher;
import com.google.re2j.Pattern;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.json.JSONObject;
import org.openide.filesystems.FileObject;
import org.yaml.snakeyaml.Yaml;

/**
 * This wraps the violation validation since the library used raises exceptions and the translator serves as exactly
 * that, in order to map the schema violations that are detected.
 *
 * @author Gavin Boshoff
 */
public class YamlSchemaViolationTranslator {

    private static final Logger LOGGER = Logger.getLogger(YamlSchemaViolationTranslator.class.getName());

    private static final Pattern TKN_EXTR_KEY_PTRN = Pattern.compile("extraneous key \\[(.*)\\]");
    private static final Pattern TKN_SPLIT_PTRN = Pattern.compile("/");

    private final YamlSchemaDefinitionManager schemaManager;
    private final Yaml yamlHandler;

    /**
     * Creates a new violation translator providing it with access to the schema manager.
     *
     * @param schemaManager The schema manager
     */
    YamlSchemaViolationTranslator(YamlSchemaDefinitionManager schemaManager) {
        this.schemaManager = schemaManager;
        this.yamlHandler = new Yaml();
    }

    /**
     * Requests for the underlying {@code yaml} file from the {@code FileObject} to be processed and checked for
     * violations and these violations be translated to the appropriate violation details that can be used to create
     * annotations in the IDE.
     *
     * @param file The file object representing an underlying {@code yaml} file
     *
     * @return The list of violations representing the violation details to be presented in the IDE
     */
    @SuppressWarnings("UseSpecificCatch")
    List<Violation> translate(FileObject file) {
        List<JSONObject> yamlJsonDocs;
        Schema yamlSchema;

        try {
            yamlSchema = this.schemaManager.getSchema(file);
            yamlJsonDocs = yamlToJson(file.asText());

            return executeValidation(file, yamlSchema, yamlJsonDocs);
        } catch (Exception ex) {
            // We intentionally catch anything we did not expect to avoid disrupting the IDE
            LOGGER.log(Level.INFO, "Failed to complete the violation translation. "
                    + "Some violations may not be shown", ex);
            return Collections.emptyList();
        }
    }

    /**
     * Converts the {@code yaml} to {@code json} in order for the underlying schema validation to be able to process
     * the document. If the file contains multiple {@code yaml} documents, each will be converted to its own JSON
     * object.
     *
     * @param yamlString The {@code yaml String} to be converted
     *
     * @return The list of {@code JSONObject}s representing the one or more provided yaml documents
     */
    private List<JSONObject> yamlToJson(String yamlString) {
        Iterable<Map<String, Object>> yamlDocIter;
        List<JSONObject> yamlDocs;

        // Less overhead to synchronize than to create a new instance for every parse (as indicated by docs)
        // Generally only an issue during initial async loading of schema definitions
        synchronized (yamlHandler) {
            yamlDocIter = (Iterable) this.yamlHandler.loadAll(yamlString);
        }

        // We need to process the iterable in order for parsing to occur
        yamlDocs = new ArrayList<>();
        yamlDocIter.forEach(yamlDoc -> yamlDocs.add(new JSONObject(yamlDoc)));

        return yamlDocs;
    }

    /**
     * Triggers the validation and translation for the provided JSON object, given the validation schema and a reference
     * to the file object.
     *
     * @param file The file object reference
     * @param yamlSchema The yaml schema definition
     * @param jsonObjects The list of json objects representing the original yaml documents
     *
     * @return The list of violations
     *
     * @throws IOException If there is an issue while validating or translating the docIdx violations
     */
    private List<Violation> executeValidation(FileObject file, Schema yamlSchema, List<JSONObject> jsonObjects)
            throws IOException {
        List<DocumentData> documents = null;
        List<Violation> allViolations;

        allViolations = new ArrayList<>();
        for (int docIdx = 0; docIdx < jsonObjects.size(); docIdx++) {

            try {
                yamlSchema.validate(jsonObjects.get(docIdx));
            } catch (ValidationException ve) {
                List<Violation> docViolations;

                docViolations = new ArrayList<>();

                extractViolation(ve, docViolations, docIdx);

                if (!docViolations.isEmpty()) {
                    // Don't build the docIdx list until it is needed
                    if (documents == null) {
                        documents = buildDocuments(file, jsonObjects.size());
                    }

                    mapViolations(documents, docViolations);
                    allViolations.addAll(docViolations);
                }
            }
        }

        return allViolations;
    }

    /**
     * The violations are provided as an exception, potentially containing further exceptions. This method processes
     * these nested exceptions recursively and adds the originating violation details to the provided list.
     *
     * @param ve The violation exception to be extracted
     * @param violations The list of violations being populated
     */
    private void extractViolation(ValidationException ve, List<Violation> violations, int documentIdx) {
        if (ve == null) return;

        if (ve.getCausingExceptions().isEmpty()) {
            Violation violation;

            violation = new Violation();
            violation.setMessage(ve.getErrorMessage());
            violation.setPointer(ve.getPointerToViolation());
            violation.setDocIdx(documentIdx);

            violations.add(violation);
        } else {
            ve.getCausingExceptions()
                    .forEach(causingEx -> extractViolation(causingEx, violations, documentIdx));
        }
    }

    /**
     * Builds the list of internal documents. In most cases, this will be returning a list with a single item. However,
     * for YAML files that consist of multiple documents, the list will contain an entry for each of these documents.
     *
     * @param file The yaml file object from which to extract the documents
     * @param documentCount The expected number of documents to be found
     *
     * @return The list containing each of the documents found within the yaml file object
     *
     * @throws IOException If there is an error reading from the file object
     */
    private List<DocumentData> buildDocuments(FileObject file, int documentCount) throws IOException {
        List<DocumentData> documents;
        DocumentData document;

        documents = new ArrayList<>();
        document = new DocumentData(0);
        documents.add(document);

        if (documentCount > 1) {
            List<String> yamlLines;

            yamlLines = new ArrayList<>(file.asLines());
            for (int lIdx = 0; lIdx < yamlLines.size(); lIdx++) {
                String line = yamlLines.get(lIdx);
                if (isDocumentMarker(line)) {
                    document = new DocumentData(lIdx + 1);
                    documents.add(document);
                } else {
                    document.addLine(line);
                }
            }
        } else {
            document.addLines(file.asLines());
        }

        // We should always have the exact number of documents, or 1 extra. An extra docIdx is due to the first
        // actual docIdx being prefixed by the docIdx marker. If we have 1 extra, we remove the first
        if (documents.size() - documentCount == 1) {
            documents.remove(0);
        }

        return documents;
    }

    /**
     * Checks if the specified line is considered a document marker. Document marker lines are lines that consist of
     * 3 consecutive dashes. i.e. {@code ---}.
     *
     * @param line The line to check
     *
     * @return {@code true} if line is a document marker, {@code false} otherwise
     */
    private boolean isDocumentMarker(String line) {
        if (line.length() > 2) {
            for (int cIdx = 0; cIdx < 3; cIdx++) {
                if (line.charAt(cIdx) != '-') {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Maps the violation details provided by the validator to their appropriate locations in the file object.
     *
     * @param documents The documents extracted from the containing file object to be used to find the location of
     *      each violation
     * @param violations The violations to be mapped to their respective locations in the originating docIdx
     *
     * @throws IOException If there is an error mapping the violations to the file content
     */
    private void mapViolations(List<DocumentData> documents, List<Violation> violations) throws IOException {
        for (Violation violation : violations) {
            Iterator<Token> tokens;

            tokens = pointerTokens(violation).stream().map(Token::new).iterator();
            if (!tokens.hasNext()) {
                continue;
            }

            DocumentData document;
            Token token;
            String line;

            document = documents.get(violation.getDocIdx());
            token = tokens.next();

            for (int lIdx = 0; lIdx < document.lineCount(); lIdx++) {
                line = document.line(lIdx);

                if (token.index) {
                    int matchedIndices;
                    int tknIdx;

                    tknIdx = token.idxValue;
                    matchedIndices = -1;

                    token = tokens.next();

                    do {
                        if (matchesIndexToken(line, token)) {
                            matchedIndices++;

                            if (matchedIndices < tknIdx && ++lIdx < document.lineCount() && token.anchorRef == null) {
                                line = document.line(lIdx);
                            } else if (tokens.hasNext()) {
                                Optional<String> anchorRef;

                                anchorRef = getPotentialAnchorRef(line);
                                if (anchorRef.isPresent()) {
                                    token.setAnchorRef(anchorRef.get());

                                    // The anchor position is unknown, so we need to start from the top
                                    lIdx = 0;
                                } else {
                                    break;
                                }
                            } else {
                                break;
                            }
                        } else if (++lIdx < document.lineCount()) {
                            line = document.line(lIdx);
                        } else {
                            break;
                        }
                    } while (true);

                    if (tokens.hasNext()) {
                        token = tokens.next();
                    } else {
                        violation.setLine(document.getFileLineIndex(lIdx));
                        break;
                    }
                } else if (matchesToken(line, token)) {
                    if (tokens.hasNext()) {
                        Optional<String> anchorRef;

                        anchorRef = getPotentialAnchorRef(line);
                        if (anchorRef.isPresent()) {
                            token.setAnchorRef(anchorRef.get());

                            // The anchor position is unknown, so we need to start from the top
                            lIdx = 0;
                        } else {
                            token = tokens.next();
                        }
                    } else {
                        violation.setLine(document.getFileLineIndex(lIdx));
                        break;
                    }
                }
            }
        }
    }

    /**
     * Builds a simple list of tokens pointing to the violation. This is effectively a yaml path being split into a
     * list for easier use. Some violations refer to an actual yaml element in the violation message detail. For these,
     * the additional key is added as the last element to the pointer list. The root token is ignored, since it always
     * points to the root.
     *
     * @param violation The violation for which the pointer should be added to a list
     *
     * @return
     */
    private List<String> pointerTokens(Violation violation) {
        List<String> tokens;

        tokens = new ArrayList<>(Arrays.asList(TKN_SPLIT_PTRN.split(violation.getPointer(), 0)));
        tokens.remove(0); // Remove the first pointer (#)

        // Check if there is an additional pointer in the message
        Matcher extraKey = TKN_EXTR_KEY_PTRN.matcher(violation.getMessage());
        if (extraKey.find()) {
            tokens.add(extraKey.group(1));
        }

        return tokens;
    }

    /**
     * Checks whether or not the specified line contains the specified index token. The token is already known to be
     * an index token and will not be checked again.
     *
     * @param line The {@code String} line to check
     * @param token The index token to be checked for in the line
     *
     * @return {@code true} if the index token is contained in the line, {@code false} if not
     */
    private boolean matchesIndexToken(String line, Token token) {
        char[] lineChars;
        char lc;

        lineChars = line.toCharArray();
        for (int cIdx = 0; cIdx < lineChars.length; cIdx++) {
            lc = lineChars[cIdx];

            if (lc == ' ' || lc == '\t' || Character.isWhitespace(lc)) {
                continue;
            }

            if (lc == '-' && (cIdx + 1 < lineChars.length)) {
                cIdx++;
                for (; cIdx < lineChars.length; cIdx++) {
                    lc = lineChars[cIdx];

                    if (lc == ' ' || lc == '\t' || Character.isWhitespace(lc)) {
                        continue;
                    }

                    if (lc == token.chars[0]) {
                        return tokenMatches(token, lineChars, cIdx);
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks whether or not the token is contained in the provided line. This is meant to check non-index tokens.
     *
     * @param line The line to check
     * @param token The token to be checked for in the line
     *
     * @return {@code true} if the line contains the token, {@code false} otherwise
     */
    private boolean matchesToken(String line, Token token) {
        char[] lineChars;
        char lc;

        lineChars = line.toCharArray();
        for (int cIdx = 0; cIdx < lineChars.length; cIdx++) {
            lc = lineChars[cIdx];

            if (lc == ' ' || lc == '\t' || Character.isWhitespace(lc)) {
                continue;
            }

            if (lc == '-' || lc == '#') {
                return false;
            }

            if (lc == token.chars[0]) {
                return tokenMatches(token, lineChars, cIdx);
            }
        }

        return false;
    }

    /**
     * This is a shared method for both regular and index tokens. At the point this method is called, the line is
     * already known to potentially contain the token. This method will check the actual token section of the line to
     * match it with the provided token.
     *
     * @param token The token to check for
     * @param lineChars The line to check converted to an array of characters
     * @param cIdx The character index at which to start checking for the token
     *
     * @return {@code true} if the token is matched in the line, {@code false} otherwise
     */
    private boolean tokenMatches(Token token, char[] lineChars, int cIdx) {
        if (cIdx + token.chars.length >= lineChars.length) {
            return false;
        }

        for (int tIdx = 1; tIdx < token.chars.length; tIdx++) {
            if (lineChars[cIdx + tIdx] != token.chars[tIdx]) {
                return false;
            }
        }

        char lc = lineChars[cIdx + token.chars.length];

        if (lc == ':' || lc == ' ') {
            if (token.anchorRef != null) {
                return matchesAnchorRef(token, lineChars, cIdx + token.chars.length);
            }

            return true;
        }

        return false;
    }

    /**
     * Checks if the given line matches a yaml anchor reference related to the specified token. At this point we should
     * already know that the line contains the specified token, as we only want to check the anchor reference
     * additionally.
     *
     * @param token The token containing an anchor reference
     * @param lineChars The line to be checked converted to an array of characters
     * @param cIdx The character index at which to start checking for the anchor reference in the line
     *
     * @return {@code true} if the anchor reference is matched, {@code false} otherwise
     */
    private boolean matchesAnchorRef(Token token, char[] lineChars, int cIdx) {
        char[] arChars;
        char lc;

        arChars = token.anchorRef.toCharArray();

        if (cIdx + arChars.length >= lineChars.length) {
            return false;
        }

        while ((lc = lineChars[cIdx]) != arChars[0]) {
            if (lc == ':' || lc == ' ' || lc == '&') {
                cIdx++;
            } else {
                return false;
            }
        }

        for (int arIdx = 1; arIdx < arChars.length; arIdx++) {
            if (lineChars[cIdx + arIdx] != arChars[arIdx]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Checks the specified line for a yaml anchor reference. This should only be called for matched tokens, as it is
     * only relevant when needing to follow the token path for the next token.
     *
     * @param line The line to check for a possible anchor reference
     *
     * @return An {@code Optional} either containing the anchor reference, if it was found, or nothing
     */
    private Optional<String> getPotentialAnchorRef(String line) {
        char[] lineChars;

        lineChars = line.toCharArray();
        for (int cIdx = 0; cIdx < lineChars.length; cIdx++) {
            if (lineChars[cIdx] == ':') {
                while (++cIdx < lineChars.length) {
                    switch (lineChars[cIdx]) {
                        case ' ':
                            break;
                        case '*':
                            return Optional.of(line.substring(cIdx + 1).trim());
                        default:
                            return Optional.empty();
                    }
                }

                return Optional.empty();
            }
        }

        // This is a logically unreachable statement
        return Optional.empty();
    }

    /**
     * Representation of a violation contained in the docIdx.
     */
    static class Violation {

        private String pointer;
        private String message;
        private int docIdx;
        private int line;

        private Violation() {}

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getPointer() {
            return pointer;
        }

        public void setPointer(String pointer) {
            this.pointer = pointer;
        }

        public int getDocIdx() {
            return docIdx;
        }

        public void setDocIdx(int docIdx) {
            this.docIdx = docIdx;
        }

        public int getLine() {
            return line;
        }

        public void setLine(int line) {
            this.line = line;
        }

        @Override
        public String toString() {
            return new StringBuilder("Violation{")
                    .append("pointer=").append(pointer)
                    .append(", message=").append(message)
                    .append(", document=").append(docIdx)
                    .append(", line=").append(line)
                    .append('}')
                    .toString();
        }
    }

    /**
     * Represents a single yaml document contained in a yaml file.
     */
    private static class DocumentData {

        private final List<String> lines;
        private final int startIdx;

        private DocumentData(int startIdx) {
            this.startIdx = startIdx;

            this.lines = new ArrayList<>();
        }

        private void addLine(String line) {
            this.lines.add(line);
        }

        private void addLines(List<String> lines) {
            this.lines.addAll(lines);
        }

        private int lineCount() {
            return this.lines.size();
        }

        private String line(int lineIdx) {
            return this.lines.get(lineIdx);
        }

        private int getFileLineIndex(int lineIdx) {
            return this.startIdx + lineIdx;
        }
    }

    /**
     * Representation of a token that forms part of the reference path to a violation.
     */
    private static class Token {

        private final boolean index;
        private final String token;
        private final char[] chars;

        private String anchorRef;
        private int idxValue;

        private Token(String token) {
            this.token = token;
            this.chars = tokenChars(token);
            this.index = this.isIndexToken();
        }

        private void setAnchorRef(String anchorRef) {
            this.anchorRef = anchorRef;
        }

        /**
         * Converts the given token value to a character array. This method takes care of reversing any escape functions
         * that were applied to the token string when it was first created by the validation library.
         *
         * @param token The token {@code String} to be converted
         *
         * @return The unescaped character array representation of the specified token
         */
        private char[] tokenChars(String token) {
            if (token.indexOf('~') > 0 || token.indexOf("\\") > 0) {
                // Unescape the token (watch out for double escape). See org.everit.json.schema.JSONPointer#escape
                String newToken;
                int prevLen = 0;

                newToken = token;
                while (prevLen != newToken.length()) {
                    prevLen = newToken.length();
                    newToken = newToken.replace("~1", "/")
                            .replace("~0", "~")
                            .replace("\\\"", "\"")
                            .replace("\\\\", "\\");
                }

                return newToken.toCharArray();
            } else {
                return token.toCharArray();
            }
        }

        /**
         * Determines whether or not the current token is an index token.
         *
         * @return {@code true} if an index token, {@code false} otherwise
         */
        private boolean isIndexToken() {
            int idx = 0;

            while (idx < chars.length) {
                if (chars[idx] < '0' || chars[idx] > '9') {
                    return false;
                }

                idx++;
            }

            this.idxValue = Integer.parseInt(this.token);

            return true;
        }
    }
}
