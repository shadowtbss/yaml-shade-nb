/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import com.shadowoflies.yaml.shade.nb.YamlSchemaDefinitionManager.SchemaLoadResult;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.Annotatable;
import org.openide.text.Line;

/**
 * This validator is meant to focus on the validation of {@code yaml} files being edited in the IDE and keep track of
 * the active violations associated with these files. To keep things simple, the validator is a singleton exposing
 * functions to handle a file and to get the violations associated with the file.
 *
 * @author Gavin Boshoff
 */
public class YamlSchemaValidator {

    private static final Logger LOGGER = Logger.getLogger(YamlSchemaValidator.class.getName());

    private static final YamlSchemaValidator INSTANCE = new YamlSchemaValidator();

    /**
     * Requests for the specified {@code FileObject} to be validated against its schema definition, if any.
     *
     * @param fileObject The file to be handled
     */
    static void handle(FileObject fileObject) {
        INSTANCE.validate(fileObject);
    }

    /**
     * Retrieves the violations that were found to be associated with the given {@code DataObject} and its underlying
     * {@code yaml} file.
     *
     * @param dataObject The {@code DataObject} containing the {@code yaml} file
     *
     * @return A list of schema violations, or an empty list if none were found. Never {@code null}
     */
    static List<YamlSchemaViolation> violations(DataObject dataObject) {
        return INSTANCE.getViolations(dataObject);
    }

    private final Map<DataObject, List<YamlSchemaViolation>> violationsMap;
    private final YamlSchemaViolationTranslator violationTranslator;
    private final YamlSchemaDefinitionManager schemaManager;

    /**
     * Initialize new instance.
     */
    private YamlSchemaValidator() {
        this.schemaManager = new YamlSchemaDefinitionManager();
        this.violationsMap = new HashMap<>();
        this.violationTranslator = new YamlSchemaViolationTranslator(this.schemaManager);
    }

    /**
     * Get the list of violations associated with a given {@code DataObject}, if any. These are the set of violations
     * currently considered active on the associated {@code yaml} file represented by the {@code DataObject}.
     *
     * @param dataObject The data object representing a given {@code yaml} file
     *
     * @return The set of currently active violations
     */
    private List<YamlSchemaViolation> getViolations(DataObject dataObject) {
        return violationsMap.computeIfAbsent(dataObject, k -> new ArrayList());
    }

    /**
     * Removes each of the violations currently associated with the given {@code DataObject}. It is also ensured that
     * the violations are all detached so they no longer give feedback through the IDE.
     *
     * @param dataObject The data object representing a given {@code yaml} file
     */
    private void clear(DataObject dataObject) {
        List<YamlSchemaViolation> daoViolations;

        daoViolations = getViolations(dataObject);
        daoViolations.forEach(violation -> violation.detach());
        daoViolations.clear();
    }

    /**
     * Requests for the existing violations, associated with the provided file, be cleared and any current violations
     * be built.
     *
     * @param file The file to be validated
     */
    private void validate(FileObject file) {
        DataObject dataObject;
        LineCookie lineCookie;

        try {
            dataObject = DataObject.find(file);
            lineCookie = dataObject.getLookup().lookup(LineCookie.class);

            clear(dataObject);

            if (lineCookie != null && checkSchema(dataObject, file, lineCookie)) {
                // Process the validation
                validateFile(dataObject, file, lineCookie);
            }
        } catch (DataObjectNotFoundException donfe) {
            LOGGER.log(Level.SEVERE, "Failed to retrieve data object for file: " + file, donfe);
        }
    }

    /**
     * Checks that the correct schema is used with the specified data object. Also, if the schema definition has not
     * yet been loaded from its location, this is done as well.
     *
     * If there is a schema specified in the file, but there is an error with retrieving or processing it, then a
     * violation will be generated representing this issue.
     *
     * @param dataObject The data object associated with the schema which should be loaded
     * @param file The file object associated with the schema which should be loaded
     * @param lineCookie The reference that can be used to work with the lines from the file
     *
     * @return {@code true} if the schema was successfully checked, {@code false} otherwise
     */
    private boolean checkSchema(DataObject dataObject, FileObject file, LineCookie lineCookie) {
        SchemaLoadResult loadResult;

        loadResult = schemaManager.loadSchema(file, result -> {
            if (handleAsyncLoadResult(result, dataObject, lineCookie)) {
                validateFile(dataObject, file, lineCookie);
            }
        });

        switch (loadResult) {
            case SCHEMA_LOADING:
            case NO_SCHEMA:
                return false;
            case SCHEMA_LOADED:
                return true;
            case INVALID_URL:
                addViolation(dataObject, lineCookie, 0, "Schema URL is invalid");
                return false;
            case OTHER_ERROR:
            default:
                return false;
        }
    }

    /**
     * <p>Handle the schema load results that are returned during asynchronous loading of schema definitions. These will
     * only be applicable to the possible results of a newly loaded schema definition.
     * </p>
     * <p>This function will also handle raising the applicable violation annotation, if there was an identifiable issue
     * while loading the schema definition.
     * </p>
     *
     * @param result The outcome for the schema definition load action
     * @param dataObject The data object associated with the schema which should be loaded
     * @param lineCookie The reference that can be used to work with the lines from the file
     *
     * @return {@code true} if the schema definition was successfully loaded, {@code false} otherwise
     */
    private boolean handleAsyncLoadResult(SchemaLoadResult result, DataObject dataObject, LineCookie lineCookie) {
        switch (result) {
            case SCHEMA_LOADED:
                return true;
            case FILE_ERROR:
            case FTP_ERROR:
            case HTTP_ERROR:
                addViolation(dataObject, lineCookie, 0, "Could not retrieve schema");
                return false;
            case INVALID_URL:
                addViolation(dataObject, lineCookie, 0, "Schema URL is invalid");
                return false;
            case FNF_ERROR:
                addViolation(dataObject, lineCookie, 0, "Schema not found at location");
                return false;
            default:
                return false;
        }
    }

    private void validateFile(DataObject dataObject, FileObject file, LineCookie lineCookie) {
        List<YamlSchemaViolationTranslator.Violation> violations;

        violations = this.violationTranslator.translate(file);
        violations.forEach(violation ->
                addViolation(dataObject, lineCookie, violation.getLine(), violation.getMessage()));
    }

    /**
     * Adds a violation annotation, given the provided message and line number index. This violation annotation is the
     * representation that the IDE can understand to show the appropriate message to the user.
     *
     * @param dataObject The data object containing the underlying yaml document being validated
     * @param lineCookie The internal document lines reference
     * @param line The line index on which the violation is present
     * @param message The message to show as the violation
     */
    private void addViolation(DataObject dataObject, LineCookie lineCookie, int line, String message) {
        YamlSchemaViolation violation;
        Line.Part currentPartLine;

        violation = new YamlSchemaViolation(line, message);
        getViolations(dataObject).add(violation);

        currentPartLine = lineCookie.getLineSet().getCurrent(line).createPart(0, 1);
        violation.attach(currentPartLine);

        currentPartLine.addPropertyChangeListener(new LineChangeListener(dataObject, currentPartLine, violation));
    }

    private void removeViolation(DataObject dataObject, YamlSchemaViolation violation) {
        getViolations(dataObject).remove(violation);
    }

    private class LineChangeListener implements PropertyChangeListener {

        private final YamlSchemaViolation violation;
        private final Line.Part currentLinePart;
        private final DataObject dataObject;

        private LineChangeListener(DataObject dataObject, Line.Part currentLinePart, YamlSchemaViolation violation) {
            this.currentLinePart = currentLinePart;
            this.dataObject = dataObject;
            this.violation = violation;
        }

        /**
         * Manage annotations on change event.
         *
         * @param evt current event
         */
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (Annotatable.PROP_TEXT.equals(evt.getPropertyName())
                    || evt.getPropertyName() == null) {

                this.currentLinePart.removePropertyChangeListener(this);
                this.violation.detach();
                removeViolation(this.dataObject, this.violation);
            }
        }
    }
}
