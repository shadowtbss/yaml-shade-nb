/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.Random;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * @author Gavin Boshoff
 */
public class YamlSchemaViolationTest {

    private static final Random RAND = new Random();

    private YamlSchemaViolation violationAnnotation;
    private String message;
    private int lineNumber;

    @BeforeEach
    public void initTest() {
        this.lineNumber = RAND.nextInt(100);
        this.message = "" + System.nanoTime();

        this.violationAnnotation = new YamlSchemaViolation(lineNumber, message);
    }

    @Test
    public void testGetAnnotationType() {
        assertEquals(YamlSchemaViolation.ANNOTATION_TYPE, this.violationAnnotation.getAnnotationType());
        assertNotEquals("xxx", this.violationAnnotation.getAnnotationType());
    }

    @Test
    public void testGetShortDescription() {
        assertEquals(this.message, this.violationAnnotation.getShortDescription());
        assertNotEquals("xxx", this.violationAnnotation.getShortDescription());
    }

    @Test
    public void testGetLineNumber() {
        assertEquals(this.lineNumber, this.violationAnnotation.getLineNumber());
        assertNotEquals(1, this.violationAnnotation.getLineNumber());
    }
}
