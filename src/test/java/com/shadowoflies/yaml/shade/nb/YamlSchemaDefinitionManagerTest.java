/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import com.shadowoflies.yaml.shade.nb.YamlSchemaDefinitionManager.SchemaLoadResult;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openide.filesystems.FileObject;

/**
 * @author Gavin Boshoff
 */
@ExtendWith(MockitoExtension.class)
public class YamlSchemaDefinitionManagerTest {

    @Mock
    private FileObject fileObject;

    @Test
    public void testLoadSchema_NoSchema() throws IOException {
        mockEditingFile("no-schema");

        YamlSchemaDefinitionManager schemaManager;
        SchemaLoadResult loadResult;

        schemaManager = new YamlSchemaDefinitionManager();

        loadResult = schemaManager.loadSchema(fileObject, _res -> {});

        assertEquals(SchemaLoadResult.NO_SCHEMA, loadResult);
    }

    @Test
    public void testLoadSchema_InvalidUrl() throws IOException {
        // We do not check for different cases of invalid URL's, since we rely on the underlying
        // library to have done that already

        mockEditingFile("invalid-url");

        YamlSchemaDefinitionManager schemaManager;
        SchemaLoadResult loadResult;

        schemaManager = new YamlSchemaDefinitionManager();

        loadResult = schemaManager.loadSchema(fileObject, _res -> {});

        assertEquals(SchemaLoadResult.INVALID_URL, loadResult);
    }

    @Test
    public void testLoadSchema_UsingUrl_SchemaLoading() throws IOException {
        mockEditingFile("load-from-url");

        YamlSchemaDefinitionManager schemaManager;
        SchemaLoadResult loadResult;

        schemaManager = new YamlSchemaDefinitionManager();

        loadResult = schemaManager.loadSchema(fileObject, _res -> {});

        assertEquals(SchemaLoadResult.SCHEMA_LOADING, loadResult);
    }

    @Test
    @SuppressWarnings("SleepWhileInLoop")
    public void testLoadSchema_SchemaLoaded() throws IOException, InterruptedException {
        mockEditingFile("load-from-url");

        YamlSchemaDefinitionManager schemaManager;
        SchemaLoadResult loadResult;
        AtomicInteger loadWait;

        schemaManager = new YamlSchemaDefinitionManager();
        loadWait = new AtomicInteger();

        loadResult = schemaManager.loadSchema(fileObject, _res -> {
            loadWait.incrementAndGet();
        });

        assertEquals(SchemaLoadResult.SCHEMA_LOADING, loadResult);

        while (loadWait.intValue() == 0) {
            Thread.sleep(10L);
        }

        loadResult = schemaManager.loadSchema(fileObject, _res -> {});

        assertEquals(SchemaLoadResult.SCHEMA_LOADED, loadResult);
        assertNotNull(schemaManager.getSchema(fileObject));
    }

    private void mockEditingFile(String lineSet) throws IOException {
        List<String> lines;

        lines = Files.readAllLines(TestUtil.getSampleFile(lineSet).toPath());

        when(fileObject.asLines()).thenReturn(lines);
    }
}
