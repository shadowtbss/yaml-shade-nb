/*
 * Copyright 2021 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shadowoflies.yaml.shade.nb;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.openide.filesystems.FileObject;

/**
 * @author Gavin Boshoff
 */
@ExtendWith(MockitoExtension.class)
public class CommonUtilTest {

    @Mock
    private FileObject fileObject;

    @ParameterizedTest
    @ValueSource(strings = {"yaml", "YAML", "Yaml", "YamL", "YAml", "yml", "YML", "YMl", "Yml", "YmL", "yML", "yMl"})
    @DisplayName("CommonUtilTest#isYamlFile - Test positive cases for checking yaml file extension")
    public void testIsYamlFile(String fileExt) {
        when(fileObject.getExt()).thenReturn(fileExt);

        assertTrue(CommonUtil.isYamlFile(fileObject));

        verify(fileObject, times(1)).getExt();
    }

    @DisplayName("CommonUtilTest#isYamlFile - Test negative case for null file object")
    public void testIsYamlFile_Null() {
        assertFalse(CommonUtil.isYamlFile(null));
    }

    @DisplayName("CommonUtilTest#isYamlFile - Test negative case for folder file object")
    public void testIsYamlFile_Folder() {
        when(fileObject.isFolder()).thenReturn(true);

        assertFalse(CommonUtil.isYamlFile(fileObject));

        verify(fileObject, times(1)).isFolder();
        verifyNoMoreInteractions(fileObject);
    }
}
