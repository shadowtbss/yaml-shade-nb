# YAML Schema Validation

NetBeans IDE plugin supporting YAML Schema Validation


## Requirements and Installation

This plugin requires **NetBeans 12.2** or above running on **JDK 8** or above.

### Manual Installation
1. Download desired `nbm` package from the [releases page](https://bitbucket.org/shadowtbss/yaml-shade-nb/downloads/)
2. Go to *Tools > Plugins > Downloaded* and select *Add Plugins*
3. Locate and select the `nbm` file previously downloaded and then *Install*

### Plugin Portal Update Center
1. **Note:** May not be available if not yet verified. In this case follow manual method (above) instead
2. Go to *Tools > Plugins > Available Plugins*
3. Find and select plugin *YAML Schema Validation* and select *Install*

### [Official NetBeans Plugin Catalog](https://plugins.netbeans.apache.org)


## Features

-  Does **NOT** override existing NetBeans YAML semantic validation
-  Validate YAML schema according to associated schema
-  Provide schema managed YAML template (*New -> Other -> YAML File (Schema Managed)*)


## Getting Started

-  Plugin installation automatically enables the feature on all yaml files
-  Enable YAML Schema Validation
    -  Specify the schema as comment in the YAML file (preferably at the top)
    -  Format chosen based on existing VSCode extension in order to improve IDE compatibility
-  Available Schemas
    -  Attempting to support all schemas provided through the [JSON Schema Store](http://schemastore.org/json/)
    -  Should also work with custom schema definitions
-  Sample

```yaml
# yaml-language-server: $schema=<urlToTheSchema>

keyOne: value
...
```


## How it Works

-  YAML documents are converted to JSON documents and checked for violations
-  YAML Schema Validation is performed when saving a `yaml` file, highlighting any violations
-  To avoid overburdening the editor, validation is only performed when saving instead of key presses


## Issues and Documentation

### Note:

-  This plugin relies on some dependencies, so please check here to know where an issue should be logged
-  If unsure what is causing a problem, create an issue against the plugin and we can advise if it needs to be logged elsewhere

### YAML Parsing

-  `YAML` parsing is performed by [SnakeYAML](https://github.com/asomov/snakeyaml)
-  Log parsing issues [here](https://bitbucket.org/asomov/snakeyaml/issues)

### YAML Violation Checking

-  `YAML` documents are checked for violations using [JSON Schema Validator](https://github.com/everit-org/json-schema)
-  Log incorrect violation detection [here](https://github.com/everit-org/json-schema/issues)

### Anything Else

-  Anything else is likely related to this plugin
-  [Create an Issue](https://bitbucket.org/shadowtbss/yaml-shade-nb/issues) with these details
    -  NetBeans version
    -  Plugin version
    -  Issue/Request description
    -  Simple `yaml` sample that can be used to re-produce the issue or serves as example


## Contributions

-  Contributions are welcome
    - No guidelines have been established yet, so try and stick to the existing conventions


## License

The plugin and its source code are licensed under [Apache 2.0 license](http://www.apache.org/licenses/LICENSE-2.0).


## Acknowledgements

Specification format for YAML schema taken from [Red Hat VS Code plugin](https://github.com/redhat-developer/vscode-yaml)

Maintainers of [SnakeYAML](https://github.com/asomov/snakeyaml)

Maintainers of [JSON Schema Validator](https://github.com/everit-org/json-schema)


## Changelog

See [the separate file](CHANGELOG.md)
