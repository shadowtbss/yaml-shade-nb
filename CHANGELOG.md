# Changelog

-  **Upcoming Releases**
    -  Fix support for list object violations

-  **0.9** (January 2022): Bug fix
    - Fix mime type to allow NetBeans to search yaml files in global searches

-  **0.8** (October 2021): Improvements
    -  Add support for multiple YAML documents contained in a single YAML file
    -  Show more relevant messages when local schema definition retrieval errors occur
    -  Load schema definitions asynchronously when retrieved for the first time
    -  Started adding tests to ensure stability
    -  Minor bugfixes

-  **0.7** (September 2021): Official first release
    -  Add support for YAML anchors and following anchor references
    -  Support loading schema files using the `file` and `ftp` protocols
    -  Change logging to reduce IDE interruptions
    -  Now publishing artifacts to [Downloads page](https://bitbucket.org/shadowtbss/yaml-shade-nb/downloads/)
    -  Add JavaDoc to code for completeness

-  **0.1** (September 2021): Unofficial initial release
